const passportJwt = require('passport-jwt'),
      mongoose = require('mongoose')
    //   googlePlusTokenStrategy = require('passport-google-plus-token'),
    //   facebookTokenStrategy = require('passport-facebook-token'),
      //instagramStrategy = require('passport-instagram').Strategy;

const JWTStrategy = passportJwt.Strategy,
      ExtractJWT = passportJwt.ExtractJwt,
      User = mongoose.model('users'),
      opts = {};

opts.jwtFromRequest = ExtractJWT.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.JWT_LOGIN_KEYS;

module.exports = passport => {
    passport.use(new JWTStrategy(opts, (jwt_payload, done) => {
        User.findById(jwt_payload.id)
            .then(user => {
                if(user){
                    return done(null, user)
                }else{
                    return done(null, false)
                }
            })
            .catch(err => console.error(err))
    }))
    
// //Google Oauth
//     passport.use('googleToken', new googlePlusTokenStrategy({
//         clientID: process.env.GOOGLE_CLIENT_ID,
//         clientSecret:process.env.GOOGLE_CLIENT_SECRET},
//     (accessToken, refreshToken, profile, done) => {
//         //check if user exist
//         User.findOne({ $or : [{"local.email":profile.emails[0].value},{"google.email":profile.emails[0].value},{"facebook.email":profile.emails[0].value}]})
//         .then( user => {
//             if(user){
//                 return done( null, user)
//             }
//                     const newUser = new User({
//                         method:'google',
//                         google:{
//                             id:profile.id,
//                             email: profile.emails[0].value,
//                             firstname: profile.name.givenName,
//                             lastname: profile.name.familyName,
//                             avatar:profile.photos[0].value,
//                             password:''
//                         },
//                         phone:'',
//                         balance:0,
//                         purchaseHistory:[],
//                         savedContacts:[]
//                     })
//                     newUser.save()
//                     .then(user => {
//                         res.json(user)
//                     })
//                     done(null, newUser);
            
//         })
//         .catch (err => res.json(err))  
//     } 
//     ))

//     // facebook
//     passport.use('facebookToken', new facebookTokenStrategy({
//         clientID:process.env.FACEBOOK_CLIENT_ID,
//         clientSecret:process.env.FACEBOOK_CLIENT_SECRET
//     }, (accessToken, refreshToken, profile, done) => {
//         User.findOne({ $or : [{"local.email":profile.emails[0].value},{"facebook.email":profile.emails[0].value},{"google.email":profile.emails[0].value}]})
//             .then(user => {
//                 if(user){
//                     return done( null, user)
//                 }
//                 const newUser = new User({
//                     method:'facebook',
//                     facebook:{
//                         id:profile.id,
//                         email: profile.emails[0].value,
//                         firstname: profile.name.givenName,
//                         lastname: profile.name.familyName,
//                         avatar:profile.photos[0].value,
//                         password:''
//                     },
//                     phone:'',
//                     balance:0,
//                     purchaseHistory:[],
//                     savedContacts:[]
//                 })
//                     newUser.save()
//                     .then(user => {
//                         res.json(user)
//                     })
//                 done(null, newUser);
//             })
//             .catch (err => res.json(err))
//     }))

//      // Instagram
//      passport.use('instagramToken', new instagramStrategy({
//         clientID:process.env.INSTAGRAM_CLIENT_ID,
//         clientSecret:process.env.INSTAGRAM_CLIENT_SECRET,
//         callbackURL: "desolate-plains-25537.herokuapp.com/" 
//     }, async(accessToken, refreshToken, profile, done) => {
//         try{ 
//             //check if user exist
//             const existingUser = await User.findOne({'instagram.id': profile.id})
//             if(existingUser){
//                 return done( null, existingUser)
//             }
//             const newUser = new User({
//                 method:'instagram',
//                 instagram:{
//                     id:profile.id,
//                     email: profile.emails[0].value,
//                     firstname: profile.name.givenName,
//                     lastname: profile.name.familyName,
//                     avatar:profile.photos[0].value,
//                     password:''
//                 },
//                 phone:'',
//                 balance:0,
//                 purchaseHistory:[],
//                 savedContacts:[]
//             })
//             await newUser.save()
//             done(null, newUser);
//         }
//         catch{
//             done(error, false, error.message)
//         }
//     }))
}

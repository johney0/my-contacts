
const express = require('express'),
validateContact = require('../validation/contact')
User = require('../models/users'),
router = express.Router(),
axios = require('axios')



router.get('/contacts/:id', (req,res) => {

User.findById(req.params.id)
.sort({date:-1})
.then(user => res.json(user.Contacts))
})

router.post('/newContact/:id', (req,res) => {
    const {errors, isValid} = validateContact(req.body) ;
    if(!isValid) return res.status(400).json(errors);
    
User.findById(req.params.id)
  .then( user => {
      user.Contacts.unshift(req.body)
          .then(user.save().then(res.json(user.Contacts)).catch(err => res.json(err)))
          .catch(err => res.json(err))
  })
  .catch(err => res.json(err))
})

router.delete('/deletecontact/:userId/:contactId', (req, res) => {
 
  User.findByIdAndUpdate(req.params.userId, { $pull: { Contacts: { "_id": req.params.contactId } } }, { new: true }).then( user => res.json(user));

})

router.put('/updatecontact/:userId/:contactId', (req,res)=>{
  User.updateOne(
    { _id: req.params.userId, "Contacts._id": req.params.contactId },
    { $set: { "Contacts.1.emailAddress" : req.body.emailAddress , "Contacts.1.phoneNumber" : req.body.phoneNumber, "Contacts.1.name" : req.body.name} }, { new: true}, function(error, doc) {
      res.json(doc)
    }
 )
})

module.exports = router;


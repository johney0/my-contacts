const express = require('express'),
      gravatar = require('gravatar'),
      bcrypt = require('bcryptjs'),
      jwt = require('jsonwebtoken'),
      passport= require('passport'),
      validateRegisteredInput = require('../validation/register'),
      validateLoginInput = require('../validation/login'),
      User = require('../models/users')



const router = express.Router()


router.post('/register', (req,res) => {
    const {errors, isValid} = validateRegisteredInput(req.body)

    if(!isValid){
        return res.status(400).json(errors)
    }
    User.findOne({ $or : [{"email":req.body.email},{"email":req.body.email},{"email":req.body.email}]})
    .then(user => {
        if(user){
            return res.status(400).json({
                email:'Email already exists'
            })
        }else{
            const avatar = gravatar.url(req.body.email, {
                s:'200',
                r:'pg',
                d:'mm'
            });
            const newUser = new User({
                method: 'local',
                    firstname:req.body.firstname,
                    lastname:req.body.lastname,
                    email:req.body.email,
                    password:req.body.password,
                    avatar
                ,
                phone:'',
                Contacts:[]
            })
            
            bcrypt.genSalt(10, (err, salt) => {
                if(err)console.error('There was an error', err)
                else{
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if(err) console.error('There was an error', err)
                        else{
                            newUser.password = hash;
                            newUser
                                .save()
                                .then(user => {
                                    res.json(user)
                                })
                        }
                    })
                }
            })
        }
    })
})

router.post('/login', (req, res) => {
    const {errors, isValid} = validateLoginInput(req.body);

    if(!isValid) return res.status(400).json(errors);
    
    const email = req.body.email,
          password =  req.body.password;
        
    User.findOne({"email":email})
        .exec()
        .then(user => {
            if(!user){
                errors.login = 'Please provide a valid username and password.'
                return res.status(401).json(errors)
            }

            bcrypt.compare(password, user.password)
                .then(isMatch => {
                    if(isMatch){
                        const payload = {
                            _id:user._id,
                            method: 'local',
                            firstname:user.firstname,
                            lastname:user.lastname,
                            email:user.email,
                            password:user.password,
                            avatar:user.avatar,
                            phone:user.phone,
                            balance:user.balance,
                            Contacts:user.Contacts
                        }
                        jwt.sign(payload, process.env.JWT_LOGIN_KEYS, {
                            expiresIn:"1h"
                        }, (err, token) => {
                            if(err) console.error('There are some errors in token', err)
                            else{
                                res.status(200).json({
                                    success:true,
                                    token:`Bearer ${token}`
                                })
                            }
                        })
                    }else{
                        errors.login = 'Please provide a valid username and password.'
                        return res.status(401).json(errors)
                    }   
                }).catch(err =>{
                    res.status(500).json({error:err})
                })
        })
    
})

// router.post('/oauth/google', passport.authenticate('googleToken', {session:false}), (req, res)=> {
//     const payload = req.user
//     jwt.sign(payload.toJSON(), process.env.JWT_LOGIN_KEYS, {
//         expiresIn:"1h"
//     }, (err, token) => {
//         if(err) console.error('There are some errors in token', err)
//         else{
//             res.status(200).json({
//                 success:true,
//                 token:`Bearer ${token}`
//             })
//         }
//     })
// })

// router.post('/oauth/facebook', passport.authenticate('facebookToken', {session:false}), (req, res)=> {
//     const payload = req.user
//     jwt.sign(payload.toJSON(), process.env.JWT_LOGIN_KEYS, {
//         expiresIn:"1h"
//     }, (err, token) => {
//         if(err) console.error('There are some errors in token', err)
//         else{
//             res.status(200).json({
//                 success:true,
//                 token:`Bearer ${token}`
//             })
//         }
//     })
// })

// router.get('/oauth/instagram', passport.authenticate('instagramToken', {session:false}), (req, res)=> {
//     const payload = req.user
//     jwt.sign(payload.toJSON(), process.env.JWT_LOGIN_KEYS, {
//         expiresIn:"1h"
//     }, (err, token) => {
//         if(err) console.error('There are some errors in token', err)
//         else{
//             res.status(200).json({
//                 success:true,
//                 token:`Bearer ${token}`
//             })
//         }
//     })
// })

router.get('/:id', (req,res) => {
    User.findById(req.params.id)
    .exec()
    .then(user => res.json(user))
    .catch(err => {
        res.status(500).json({error:err})
    })
}) 
router.get('/', (req, res) => {
    User.find()
    .sort({date:-1})
    .select('firstname lastname email _id')
    .exec()
    .then(users => {
        const response = {
            count: users.length,
            users: users.map(user => {
                return {
                    user, request: {
                        type:'GET',
                        url:'http://localhost:5000/api/users/' + user._id
                    }
                }
            })

        }
        res.json(response)
    })
})

router.get('/me', passport.authenticate('jwt', {session:false}), (req,res) => {
    return res.json({
        id:req.user.id,
        name:req.user.name,
        email:req.user.email,
        Contacts:req.user.Contacts
    })
})

module.exports = router;
const mongoose = require('mongoose')
const Schema = mongoose.Schema;


const Contacts = new Schema ({
    name:{
        type:String
    },
    emailAddress:{
        type:String
    },
    phoneNumber:{
        type:String
    }
})

const userSchema = new Schema ({
    method:{
        type:String,
        enum: ['local', 'google', 'facebook'],
        required: true
    },
    firstname: {
            type: String,
    },
    lastname: {
            type: String,
        },
    password: {
            type: String,
        },
    id:{
        type:String
    },
    email: {
        type: String,
        lowecase:true
    },
    avatar: {
        type: String
    },
    Contacts:[Contacts],
    phone:{
        type:String
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = User = mongoose.model('users', userSchema)
const validator = require('validator'),
      isEmpty = require('./isEmpty')

module.exports = validateContact = (data) => {
    let errors = {};
    data.name = !isEmpty(data.name) ? data.name : '';
    data.phoneNumber = !isEmpty(data.phoneNumber) ? data.phoneNumber : '';



    if(validator.isEmpty(data.phoneNumber)){
        errors.phoneNumber = 'A phone number is required'
    }
    if(!validator.isMobilePhone( data.phoneNumber,'en-NG')){
        errors.phoneNumber = 'Not a valid phone number'
    }

    if(!validator.isLength(data.name, {min:2, max:30})){
        errors.name = `name must be between 2 to 30`
    }
    if(validator.isEmpty(data.name)){
        errors.name = 'name field is required'
    }

    return {
        errors,
        isValid:isEmpty(errors)
    }
}
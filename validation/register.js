const validator = require('validator'),
      isEmpty = require('./isEmpty')

module.exports = validateRegisterInput = (data) => {
    let errors = {};
    data.firstname = !isEmpty(data.firstname) ? data.firstname : '';
    data.lastname = !isEmpty(data.lastname) ? data.lastname : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';
    data.password_confirm = !isEmpty(data.password_confirm) ? data.password_confirm : '';

    if(!validator.isLength(data.firstname, {min:2, max:30})){
        errors.firstname = `First name must be between 2 to 30`
    }
    if(validator.isEmpty(data.firstname)){
        errors.firstname = 'First name field is required'
    }
    if(!validator.isLength(data.lastname, {min:2, max:30})){
        errors.lastname = `Last name must be between 2 to 30`
    }
    if(validator.isEmpty(data.lastname)){
        errors.lastname = 'Last name field is required'
    }
    if(!validator.isEmail(data.email)){
        errors.email = 'Email is invalid'
    }
    if(validator.isEmpty(data.email)){
        errors.email = 'Email is required'
    }
    if(!validator.isLength(data.password, {min:6, max:30})){
        errors.password = `Password must have 6 characters`
    }
    if(validator.isEmpty(data.password)){
        errors.password = 'Password is required'
    }
    if(!validator.isLength(data.password_confirm, {min:6, max:30})){
        errors.password_confirm = `Password must have 6 characters`
    }
    if(!validator.equals(data.password, data.password_confirm)){
        errors.password_confirm = `Password must match`
    }
    if(validator.isEmpty(data.password_confirm)){
        errors.password_confirm= 'Password is required'
    }

    return {
        errors,
        isValid:isEmpty(errors)
    }
}
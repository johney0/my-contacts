import { combineReducers } from "redux";
import ContactReducers from "./ContactReducers";
import errorReducers from "./errorReducers";
import authReducers from "./authReducers";

export default combineReducers({
  Contact: ContactReducers,
  errors: errorReducers,
  auth: authReducers
});

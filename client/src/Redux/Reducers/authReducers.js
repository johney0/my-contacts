import {
  SET_CURRENT_USER,
  ADD_CONTACT,
  DELETE_CONTACT,
  MODALS_LOADING,
  EDIT_CONTACT,
  ON_CHANGE,
  UPDATE_CONTACT
} from "../Actions/Types";
import isEmpty from "../isEmpty";
const initialState = {
  isAuthenticated: false,
  User: {},
  modal: false,
  isEdit: 0,
  name: "",
  phoneNumber: "",
  emailAddress: "",
  checked: false
};
export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        User: action.payload,
        loader: false
      };

    case ADD_CONTACT:
      return {
        ...state,
        User: {
          ...state.User,
          Contacts: action.payload
        },
        name: "",
        phoneNumber: "",
        emailAddress: "",
        checked: false,
        isEdit: 0,
        modal: false
      };
    case DELETE_CONTACT:
      return {
        ...state,
        User: {
          ...state.User,
          Contacts: state.User.Contacts.filter(
            contact => contact._id !== action.payload
          )
        }
      };
    case EDIT_CONTACT:
      return {
        ...state,
        isEdit: action.payload._id,
        modal: true,
        name: action.payload.name,
        phoneNumber: action.payload.phoneNumber,
        emailAddress: action.payload.emailAddress,
        checked: action.payload.checked
      };
    case UPDATE_CONTACT:
      return {
        ...state,
        User: {
          ...state.User,
          Contacts: state.User.Contacts.map(contact => {
            if (contact._id === action.payload.updatedContact) {
              return {
                _id: action.payload.updatedContact,
                name: action.payload.newContact.name,
                phoneNumber: action.payload.newContact.phoneNumber,
                emailAddress: action.payload.newContact.emailAddress
              };
            } else return contact;
          })
        },
        isEdit: 0,
        phoneNumber: "",
        emailAddress: "",
        name: "",
        modal: false
      };
    case MODALS_LOADING:
      return {
        ...state,
        modal: !state.modal
      };
    case ON_CHANGE:
      return {
        ...state,
        [action.payload.target.name]: action.payload.target.value
      };
    default:
      return state;
  }
}

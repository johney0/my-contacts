import {
  GET_CONTACTS,
  ADD_CONTACT,
  DELETE_CONTACT,
  EDIT_CONTACT,
  CONTACTS_LOADING,
  UPDATE_CONTACT,
  ON_CHANGE
} from "./Types";
import axios from "axios";

export const getContacts = () => dispatch => {
  dispatch(loading());
  axios.get("./api/contacts").then(res =>
    dispatch({
      type: GET_CONTACTS,
      payload: res.data
    })
  );
};

export const deleteContact = (id, contact) => dispatch => {
  axios.delete(`/api/users/deletecontact/${id}/${contact}`).then(res =>
    dispatch({
      type: DELETE_CONTACT,
      payload: contact
    })
  );
};

export const editContact = Contactdcard => {
  return {
    type: EDIT_CONTACT,
    payload: Contactdcard
  };
};

export const addContact = (id, newContact) => dispatch => {
  axios.post(`/api/users/newContact/${id}`, newContact).then(res =>
    dispatch({
      type: ADD_CONTACT,
      payload: res.data
    })
  );
};

export const updateContact = (id, updatedContact, newContact) => dispatch => {
  console.log(newContact)
  axios
    .put(`/api/users/updatecontact/${id}/${updatedContact}`, newContact)
    .then(res =>
      dispatch({
        type: UPDATE_CONTACT,
        payload: { newContact, updatedContact }
      })
    );
};

export const loading = () => {
  return {
    type: CONTACTS_LOADING
  };
};
export const onChange = e => {
  return {
    type: ON_CHANGE,
    payload: e
  };
};

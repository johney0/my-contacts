import {
  GET_ERRORS,
  SET_CURRENT_USER,
  CLEAR_ERRORS,
  MODALS_LOADING
} from "./Types";
import axios from "axios";
import setAuthToken from "../setAuthToken";
import jwt_decode from "jwt-decode";

export const registerUser = (user, history) => dispatch => {
  axios
    .post("/api/users/register", user)
    .then(res => history.push("/login-page"))
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

export const loginUser = (user, history) => dispatch => {
  //dispatch(loader());
  axios
    .post("/api/users/login", user)
    .then(res => {
      const { token } = res.data;
      localStorage.setItem("jwtToken", token);
      setAuthToken(token);
      const decoded = jwt_decode(token);
      dispatch(setCurrentUser(decoded));
      history.push("/admin/dashboard");
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

// //Google Oauth

// export const oauthGoogle = (data, history) => dispatch => {
//   axios.post("/api/users/oauth/google", { access_token: data }).then(res => {
//     const { token } = res.data;
//     localStorage.setItem("jwtToken", token);
//     setAuthToken(token);
//     const decoded = jwt_decode(token);
//     dispatch(setCurrentUser(decoded));
//     history.push("/profile-page");
//   });
// };
// //facebook Oauth
// export const oauthFacebook = (data, history) => dispatch => {
//   axios.post("/api/users/oauth/facebook", { access_token: data }).then(res => {
//     const { token } = res.data;
//     localStorage.setItem("jwtToken", token);
//     setAuthToken(token);
//     const decoded = jwt_decode(token);
//     dispatch(setCurrentUser(decoded));
//     history.push("/profile-page");
//   });
// };

export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

export const logoutUser = history => dispatch => {
  localStorage.removeItem("jwtToken");
  setAuthToken(false);
  dispatch(setCurrentUser({}));
  history.push("/login-page");
};

export const newModal = () => {
  return {
    type: MODALS_LOADING
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS,
    payload: {}
  };
};

/* eslint-disable react/prop-types */
import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Checkbox from "@material-ui/core/Checkbox";
import Check from "@material-ui/icons/Check";
import Tooltip from "@material-ui/core/Tooltip";
import Edit from "@material-ui/icons/Edit";
import IconButton from "@material-ui/core/IconButton";
import Close from "@material-ui/icons/Close";
import tasksStyle from "assets/jss/material-dashboard-react/components/tasksStyle.jsx";
import { connect } from "react-redux";

// core components
import tableStyle from "assets/jss/material-dashboard-react/components/tableStyle.jsx";
import { cpus } from "os";
import { deleteContact, editContact } from "../../Redux/Actions/ContactActions";

class CustomTable extends React.Component {
  state = {
    checked: []
  };
  handleToggle = value => () => {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked
    });
  };
  editContact = contacts => {
    console.log(contacts)
    this.props.editContact(contacts);
  };
  deleteContact = id => {
    this.props.deleteContact(this.props.User._id, id);
  };

  render() {
    const { classes, tableHead, tableData, tableHeaderColor } = this.props;

    return (
      <div className={classes.tableResponsive}>
        <Table className={classes.table}>
          {tableHead !== undefined ? (
            <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
              <TableRow>
                {tableHead.map((prop, key) => {
                  return (
                    <TableCell
                      className={
                        classes.tableCell + " " + classes.tableHeadCell
                      }
                      key={key}
                    >
                      {prop}
                    </TableCell>
                  );
                })}
              </TableRow>
            </TableHead>
          ) : null}
          <TableBody>
            {tableData.map(prop => {
              return (
                <TableRow key={prop._id} className={classes.tableRow}>
                  <TableCell className={classes.tableCell}>
                    <Checkbox
                      checked={this.state.checked.indexOf(prop.id) !== -1}
                      tabIndex={-1}
                      onClick={this.handleToggle(prop.id)}
                      checkedIcon={<Check className={classes.checkedIcon} />}
                      icon={<Check className={classes.uncheckedIcon} />}
                      classes={{
                        checked: classes.checked,
                        root: classes.root
                      }}
                    />
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.name}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.phoneNumber}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {prop.emailAddress}
                  </TableCell>

                  <TableCell className={classes.tableActions}>
                    <Tooltip
                      id="tooltip-top"
                      title="Edit Task"
                      placement="top"
                      classes={{ tooltip: classes.tooltip }}
                    >
                      <IconButton
                        aria-label="Edit"
                        className={classes.tableActionButton}
                        onClick={()=> this.editContact(prop)}
                      >
                        <Edit
                          className={
                            classes.tableActionButtonIcon + " " + classes.edit
                          }
                        />
                      </IconButton>
                    </Tooltip>
                    <Tooltip
                      id="tooltip-top-start"
                      title="Remove"
                      placement="top"
                      classes={{ tooltip: classes.tooltip }}
                    >
                      <IconButton
                        aria-label="Close"
                        className={classes.tableActionButton}
                        onClick={()=> this.deleteContact(prop._id)}
                      >
                        <Close
                          className={
                            classes.tableActionButtonIcon + " " + classes.close
                          }
                        />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
    );
  }
}

CustomTable.defaultProps = {
  tableHeaderColor: "gray"
};

CustomTable.propTypes = {
  classes: PropTypes.object.isRequired,
  tableHeaderColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray"
  ]),
  tableHead: PropTypes.arrayOf(PropTypes.string),
  tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};

const mapStateToProps = state => ({
  errors: state.errors,
  auth: state.auth
});

export default withStyles(tasksStyle)(
  connect(
    mapStateToProps,
    { deleteContact, editContact }
  )(CustomTable)
);

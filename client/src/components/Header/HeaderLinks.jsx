/*eslint-disable*/
import React from "react";
import PropTypes from 'prop-types'
// react components for routing our app without refresh
import { Link, withRouter } from "react-router-dom";
//Redux
import { connect } from 'react-redux'
import { logoutUser } from '../../Redux/Actions/authentication'
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";


import Button from "components/CustomButtons/Button.jsx";

import headerLinksStyle from "assets/jss/material-dashboard-react/components/headerLinksStyle.jsx";

class HeaderLinks extends React.Component {
  constructor({...props}){
    super({...props})
  }
  onLogoutUser = e => {
    e.preventDefault()
    this.props.logoutUser(this.props.history)
  }

  render(){
    const { classes } = this.props;
    const { isAuthenticated, User } = this.props.auth;
    const authLinks = (
      <React.Fragment>
        <ListItem className={classes.listItem}>
              <Link to = "/profile-page">
                <Button
                  className={classes.registerNavLink}
                  style={{color:'#777'}}
                  color="white"
                  round
                >
                Profile
                </Button>
                </Link>
              </ListItem>
      <ListItem className={classes.listItem}>
          <Button
            onClick = {this.onLogoutUser}
            className={classes.registerNavLink}
            color="white"
            style={{color:'#777'}}
            round
          >
          Logout
          </Button>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Button 
            className={classes.registerNavLink}
            color="transparent"
            style={{color:'#777'}}
            round>
              Balance: ₦ {balance}
          </Button>
        </ListItem>
      </React.Fragment>
    );

    const guestLinks = (
      <React.Fragment>
         <ListItem className={classes.listItem}>              
                <Link to = "/register-page">
                <Button
                className={classes.registerNavLink}
                style={{color:'#777',marginRight:"5px"}}
                  color="white"
                  round
                >
                Register
                </Button>
                </Link>   
              </ListItem>
              <ListItem className={classes.listItem}>
              <Link to = "/login-page">
                <Button
                  className={classes.registerNavLink}
                  style={{color:'#777',marginRight:"5px"}}
                  color="white"
                  round
                >
                Login
                </Button>
                </Link>
              </ListItem>
      </React.Fragment>
    )

    return(
      <List className={classes.list}>
              { isAuthenticated ? authLinks : guestLinks}
      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-twitter"
          title="Follow us on twitter"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            href="#"
            target="_blank"
            color="transparent"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-twitter"} />
          </Button>
        </Tooltip>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-facebook"
          title="Follow us on facebook"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            color="transparent"
            href="https://www.facebook.com/john.eyo1"
            target="_blank"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-facebook"} />
          </Button>
        </Tooltip>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-tooltip"
          title="Follow us on instagram"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            color="transparent"
            href="https://www.instagram.com/e.y.o_"
            target="_blank"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-instagram"} />
          </Button>
        </Tooltip>
      </ListItem>
    </List>
    )
  }
}

HeaderLinks.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth:PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth : state.auth
})
 

export default connect(mapStateToProps, {logoutUser})(withStyles(headerLinksStyle)(withRouter(HeaderLinks)))
;

/* eslint-disable react/prop-types */
import React from "react";

import { Modal, ModalHeader, ModalBody, Button } from "reactstrap";

const Modals = props => {
  const { toggle, modal, body, header, buttonLabel } = props;
  return (
    <React.Fragment>
      <Button color="danger" onClick={toggle}>
        {buttonLabel}
      </Button>
      <Modal
        style={{ marginTop: "20vh" }}
        isOpen={modal}
        toggle={toggle}
        className={props.className}
      >
        <ModalHeader toggle={toggle}> {header} </ModalHeader>
        <ModalBody>{body}</ModalBody>
      </Modal>
    </React.Fragment>
  );
};
export default Modals;

import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import store from "./Redux/store";
import jwt_decode from "jwt-decode";
import setAuthToken from "./Redux/setAuthToken";
import { setCurrentUser, logoutUser } from "./Redux/Actions/authentication";
import { Provider } from "react-redux";

import indexRoutes from "./routes/index";
import "assets/css/material-dashboard-react.css?v=1.6.0";
import "bootstrap/dist/css/bootstrap.min.css";

const hist = createBrowserHistory();

if (localStorage.jwtToken) {
  setAuthToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  store.dispatch(setCurrentUser(decoded));

  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser(hist));
    window.location.href = "/login-page";
  }
}

ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <Switch>
        {indexRoutes.map((prop, key) => {
          return (
            <Route path={prop.path} key={key} component={prop.component} />
          );
        })}
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);

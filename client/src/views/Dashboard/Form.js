/* eslint-disable react/prop-types */
import React from "react";
import { FormGroup, Label, Input, Button, Row, FormText } from "reactstrap";

export default class Forms extends React.Component {
  render() {
    const {
      onChange,
      nameValue,
      emailAddressValue,
      phoneNumberValue,
      onSubmit
    } = this.props;
    return (
      <React.Fragment>
        <FormGroup>
          <Label for="name">Name</Label>
          <Input
            type="text"
            name="name"
            value={nameValue}
            id="name"
            placeholder="Name"
            onChange={onChange}
          />
        </FormGroup>

        <FormGroup>
          <Label for="phoneNumber">Phone Number</Label>
          <Input
            type="text"
            name="phoneNumber"
            value={phoneNumberValue}
            id="phoneNumber"
            placeholder="Phone Number"
            onChange={onChange}
          />
        </FormGroup>

        <FormGroup>
          <Label for="emailAddress">Email Address</Label>
          <Input
            type="email"
            name="emailAddress"
            value={emailAddressValue}
            id="email"
            placeholder="Email"
            onChange={onChange}
          />
        </FormGroup>

        <Button
          color="dark"
          style={{ marginTop: "2rem" }}
          block
          onClick={onSubmit}
        >
          Add Item
        </Button>
      </React.Fragment>
    );
  }
}

/* eslint-disable react/prop-types */
import React from "react";
import PropTypes from "prop-types";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { newModal } from "../../Redux/Actions/authentication";
import {
  addContact,
  onChange,
  updateContact
} from "../../Redux/Actions/ContactActions";
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Table from "components/Table/Table.jsx";
import Danger from "components/Typography/Danger.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import Modal from "../../components/Modal/Modal";
import Form from "./Form";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import { Button } from "@material-ui/core";

class Dashboard extends React.Component {
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  toggle = () => {
    this.props.newModal();
  };

  handleInputChange = e => {
    this.props.onChange(e);
  };

  handleSubmit = e => {
    const { isEdit, User, name, emailAddress, phoneNumber } = this.props.auth;
    const id = User._id;
    e.preventDefault();

    const newContact = {
      emailAddress,
      name,
      phoneNumber
    };

    if (isEdit) {
      this.props.updateContact(id, isEdit, newContact);
    } else {
      this.props.addContact(id, newContact);
    }
  };

  render() {
    const { classes } = this.props;
    const {
      User,
      modal,
      name,
      phoneNumber,
      emailAddress,
      isEdit,
      checked
    } = this.props.auth;
    const { errors } = this.props.errors;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={6} md={4}>
            <Card>
              <CardHeader color="success" stats icon>
                <CardIcon color="success">
                  <Store />
                </CardIcon>
                <p className={classes.cardCategory}>Complete</p>
                <h3 className={classes.cardTitle}>3</h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <DateRange />
                  Number of completed contacts
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={4}>
            <Card>
              <CardHeader color="danger" stats icon>
                <CardIcon color="danger">
                  <Store />
                </CardIcon>
                <p className={classes.cardCategory}>Incomplete</p>
                <h3 className={classes.cardTitle}>4</h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <LocalOffer />
                  Number of incomplete contacts
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card>
              <CardHeader color="info" stats icon>
                <CardIcon color="info">
                  <Accessibility />
                </CardIcon>
                <p className={classes.cardCategory}>Contacts</p>
                <h3 className={classes.cardTitle}>45</h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <Update />
                  All Contacts
                </div>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>

        <Card>
          <CardBody>
            <Modal
              toggle={this.toggle}
              modal={modal}
              buttonLabel="Add up"
              header={"add me"}
              body={
                <Form
                  nameValue={name}
                  phoneNumberValue={phoneNumber}
                  emailAddressValue={emailAddress}
                  onSubmit={this.handleSubmit}
                  onChange={this.handleInputChange}
                  errors={errors}
                />
              }
            />
            <Table
              tableHeaderColor="warning"
              tableHead={["", "Name", "Phone Number", "Email Address"]}
              tableData={User.Contacts}
              User={User}
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors,
  auth: state.auth
});

export default withStyles(dashboardStyle)(
  connect(
    mapStateToProps,
    { newModal, addContact, onChange, updateContact }
  )(withRouter(Dashboard))
);

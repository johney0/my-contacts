import LoginPage from "../layouts/LoginPage/LoginPage";
import RegisterPage from "../layouts/RegisterPage/Registerpage";
import Admin from "../layouts/Admin";
import LandingPage from "../layouts/LandingPage";

let indexRoutes = [
  { path: "/login-page", name: "LoginPage", component: LoginPage },
  { path: "/admin", name: "Admin", component: Admin },
  { path: "/register-page", name: "RegisterPage", component: RegisterPage },
  { path: "/", name: "LandingPage", component: LandingPage }
];

export default indexRoutes;

import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

//REDUX
import { connect } from "react-redux";
import { registerUser } from "../../Redux/Actions/authentication";

// core components
import Header from "components/Header/Header.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import loginPageStyle from "../../assets/jss/material-dashboard-react/views/loginPage";
import withStyles from "@material-ui/core/styles/withStyles";

import image from "assets/img/header.jpg";
import RegisterForm from "./RegisterForm";

class RegisterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      password_confirm: "",
      errors: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/");
    }
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/");
    }
  }

  handleInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    console.log('vhh')
  };
  handleSubmit = e => {
    e.preventDefault();
    const User = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      email: this.state.email,
      password: this.state.password,
      password_confirm: this.state.password_confirm
    };
    this.props.registerUser(User, this.props.history);
  };

  render() {
    const { classes, ...rest } = this.props;
    const {
      errors,
      firstname,
      lastname,
      email,
      password,
      password_confirm
    } = this.state;
    return (
      <div>
        {/* <Header
          absolute
          color="transparent"
          brand="Airtime Hub"
          rightLinks={<HeaderLinks />}
          {...rest}
        /> */}
        <div
          className={classes.pageHeader}
          style={{
            backgroundImage: "url(" + image + ")",
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
        >
          <RegisterForm
            classes={classes}
            submit={this.handleSubmit}
            onChange={this.handleInputChange}
            errors={errors}
            firstname={firstname}
            lastname={lastname}
            emailValue={email}
            passwordValue={password}
            password_confirmValue={password_confirm}
          />
        </div>
      </div>
    );
  }
}

RegisterPage.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors,
  auth: state.auth
});
export default withStyles(loginPageStyle)(
  connect(
    mapStateToProps,
    { registerUser }
  )(withRouter(RegisterPage))
);

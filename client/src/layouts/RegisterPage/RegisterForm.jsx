import React from "react";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import { Link } from "react-router-dom";

import CardFooter from "components/Card/CardFooter.jsx";

import FormHeader from "./FormHeader";
import FormBody from "./FormBody";

class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden"
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }
  render() {
    const {
      classes,
      submit,
      onChange,
      firstname,
      lastname,
      emailValue,
      passwordValue,
      password_confirmValue,
      errors
    } = this.props;
    return (
      <div className={classes.container}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={4}>
            <Card className={classes[this.state.cardAnimaton]}>
              <form onSubmit={submit} className={classes.form}>
                <FormHeader classes={classes} />
                <p className={classes.divider}>Or Be Classical</p>
                <FormBody
                  classes={classes}
                  onChange={onChange}
                  errors={errors}
                  firstname={firstname}
                  lastname={lastname}
                  emailValue={emailValue}
                  passwordValue={passwordValue}
                  password_confirmValue={password_confirmValue}
                />
                <CardFooter className={classes.cardFooter}>
                  <GridContainer justify="center">
                    <Button type="submit" simple color="primary" size="lg">
                      Join Us
                    </Button>
                    <GridItem>
                      <GridContainer>
                        <GridItem xs={8} sm={6} md={6} style={{ padding: 0 }}>
                          <p>Already have an account ?</p>
                        </GridItem>
                        <GridItem xs={4} sm={6} md={6}>
                          <Link to="/login-page">
                            <Button
                              style={{ margin: "0 auto", padding: 0 }}
                              simple
                              color="primary"
                              size="lg"
                            >
                              LOGIN
                            </Button>
                          </Link>
                        </GridItem>
                      </GridContainer>
                    </GridItem>
                  </GridContainer>
                </CardFooter>
              </form>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default RegisterForm;

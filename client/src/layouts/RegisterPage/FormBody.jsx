/* eslint-disable react/prop-types */
import React from "react";
// @material-ui/core components
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
import CardBody from "components/Card/CardBody.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";

const formBody = props => {
  const {
    classes,
    onChange,
    firstname,
    lastname,
    emailValue,
    passwordValue,
    password_confirmValue,
    errors
  } = props;
  return (
    <CardBody>
      <CustomInput
        labelText="First Name..."
        id="firstname"
        name="firstname"
        onChange={onChange}
        value={firstname}
        formControlProps={{
          fullWidth: true
        }}
        inputProps={{
          type: "text",
          endAdornment: (
            <InputAdornment position="end">
              <People className={classes.inputIconsColor} />
            </InputAdornment>
          )
        }}
      />
      {errors.firstname && (
        <div className="text-danger">{errors.firstname}</div>
      )}

      <CustomInput
        labelText="Last Name..."
        id="lastname"
        name="lastname"
        onChange={onChange}
        value={lastname}
        formControlProps={{
          fullWidth: true
        }}
        inputProps={{
          type: "text",
          endAdornment: (
            <InputAdornment position="end">
              <People className={classes.inputIconsColor} />
            </InputAdornment>
          )
        }}
      />
      {errors.lastname && <div className="text-danger">{errors.lastname}</div>}

      <CustomInput
        labelText="Email..."
        id="email"
        name="email"
        onChange={onChange}
        value={emailValue}
        formControlProps={{
          fullWidth: true
        }}
        inputProps={{
          type: "email",
          endAdornment: (
            <InputAdornment position="end">
              <Email className={classes.inputIconsColor} />
            </InputAdornment>
          )
        }}
      />
      {errors.email && <div className="text-danger">{errors.email}</div>}

      <CustomInput
        labelText="Password"
        id="pass"
        name="password"
        onChange={onChange}
        value={passwordValue}
        formControlProps={{
          fullWidth: true
        }}
        inputProps={{
          type: "password",
          endAdornment: (
            <InputAdornment position="end">
              <Icon className={classes.inputIconsColor}>lock_outline</Icon>
            </InputAdornment>
          )
        }}
      />
      {errors.password && <div className="text-danger">{errors.password}</div>}

      <CustomInput
        labelText="Confirm Password"
        id="pass"
        name="password_confirm"
        onChange={onChange}
        value={password_confirmValue}
        formControlProps={{
          fullWidth: true
        }}
        inputProps={{
          type: "password",
          endAdornment: (
            <InputAdornment position="end">
              <Icon className={classes.inputIconsColor}>lock_outline</Icon>
            </InputAdornment>
          )
        }}
      />
      {errors.password_confirm && (
        <div className="text-danger">{errors.password_confirm}</div>
      )}
    </CardBody>
  );
};

export default formBody;

import React from "react";
import CardHeader from "components/Card/CardHeader.jsx";
import Button from "components/CustomButtons/Button.jsx";

const formHeader = props => {
  const { classes } = props;
  return (
    <CardHeader color="primary" className={classes.cardHeader}>
      <h4>Register With</h4>
      <div className={classes.socialLine}>
        {/* <Button
                          justIcon
                          href="#pablo"
                          target="_blank"
                          color="transparent"
                          onClick={e => e.preventDefault()}
                        >
                          <i className={"fab fa-twitter"} />
                        </Button>
                        */}
        <h6>SOCIALS
        </h6>
      </div>
    </CardHeader>
  );
};

export default formHeader;
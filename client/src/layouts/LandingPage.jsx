/* eslint-disable prettier/prettier */
import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { Link, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
//REDUX
import { connect } from 'react-redux';
import { clearErrors, loginUser } from '../Redux/Actions/authentication'

// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
//import './Landing.css'
// @material-ui/icons

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";

import landingPageStyle from "assets/jss/material-dashboard-react/views/LandingPage.jsx";
import LoginForm from "./LoginPage/LoginForm";


const dashboardRoutes = [];

class LandingPage extends React.Component {
  constructor(props){
    super(props)
    this.state= {
      email: '',
      password: '',
      errors: {}
    }
  }

  render() {
    const { classes, ...rest } = this.props;
    const { phoneNumber, email, errors, password, statusLoader, firstName, pk, Order} = this.state;
    const { modal, isAuthenticated, User } = this.props.auth

    return (
      <div>
        {/* <ConsecutiveSnackbars/> */}
        {/* <Header 
          color="transparent"
          routes={dashboardRoutes}
          brand="Airtime Hub"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "dark"
          }}
          {...rest}
        /> */}
        <div className="page-header">
          <Parallax filter image={require("assets/img/header.jpg")}>
            <div className={classes.container}>
            <div className="content-center brand">
          <img className="n-logo" src={require("assets/img/now-logo.png")} alt=""/>
          <h1 className="h1-seo">Airtime Hub</h1>
          <h3>{`Your one stop for 24/7 airtime supply`}</h3>
          <h6 className="category category-absolute"> 
              {isAuthenticated ?
              <React.Fragment >
              <h4>Hello {User.firstname} </h4> 
              <Link style={{margin:'0 auto'}} className='linkedLogin' to = "/admin/dashboard">
                  <Button
                    className={classes.registerNavLink}
                    color="rose"
                    round
                  >
                  Dashboard
                  </Button>
                </Link>
              </React.Fragment>        
              :
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <Link className='linkedLogin' to = "/login-page">
                  <Button
                    className={classes.registerNavLink}
                    color="rose"
                    round
                  >
                  Login
                  </Button>
                </Link>
              </GridItem>
              <GridItem xs={12} sm={12} md={6}><Link  className='linkedRegister'to = "/register-page">
                <Button
                  className={classes.registerNavLink}
                  color="rose"
                  round
                >
                Register
                </Button>
                </Link></GridItem>
              </GridContainer>}
          </h6> 
          
        </div>
      </div>
    </Parallax>
  </div>
        <Footer />
      </div>
    );
  }
  // toggle=()=>{
  //   this.props.newModal()
  // }
  // clear=()=> {
  //   this.toggle()
  //   this.setState({Order:[{}]})
  // }
}
LandingPage.propTypes = {
  classes: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
}
const mapStateToProps = state => ({
  errors: state.errors,
  auth:state.auth
})
// export default withStyles(landingPageStyle)(connect(mapStateToProps, {guestValidateAirtimePurchase, guestValidateDataPurchase, verifyStatus, succesfulPurchase, newModal, loader, clearErrors})(withRouter(LandingPage)));

export default withStyles(landingPageStyle)(connect(mapStateToProps)(withRouter(LandingPage)));

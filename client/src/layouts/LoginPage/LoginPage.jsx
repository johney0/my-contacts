/* eslint-disable react/prop-types */
import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import Header from "components/Header/Header.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Footer from "components/Footer/Footer.jsx";

//REDUX
import { connect } from "react-redux";
import { loginUser } from "../../Redux/Actions/authentication";

import loginPageStyle from "assets/jss/material-dashboard-react/views/loginPage.jsx";

import image from "assets/img/header.jpg";
import LoginForm from "./LoginForm";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      errors: {}
      //loader:false
    };
    // this.responseGoogle = this.responseGoogle.bind(this)
    // this.responseFacebook = this.responseFacebook.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/admin/dashboard");
    }
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  handleInputChange = e => {
    console.log("j");
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const User = {
      email: this.state.email,
      password: this.state.password
    };
    console.log(User);

    this.props.loginUser(User, this.props.history);
  };

  //   async responseGoogle(res){
  //     await this.props.oauthGoogle(res.accessToken, this.props.history)
  //   };

  //   async responseFacebook(res){
  //     await this.props.oauthFacebook(res.accessToken, this.props.history)
  //   }

  render() {
    const { classes, ...rest } = this.props;
    const { errors, email, password } = this.state;
    return (
      <div>
        {/* <Header
          absolute
          color="transparent"
          brand="Airtime Hub"
          rightLinks={<HeaderLinks />}
          {...rest}
        /> */}
        <div
          className={classes.pageHeader}
          style={{
            backgroundImage: "url(" + image + ")",
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
        >
          <LoginForm
            classes={classes}
            errors={errors}
            emailValue={email}
            passwordValue={password}
            onChange={this.handleInputChange}
            submit={this.handleSubmit}
            //loader= {this.state.loader}
            // responseGoogle={this.responseGoogle}
            // responseFacebook={this.responseFacebook}
          />
          <Footer whiteFont />
        </div>
      </div>
    );
  }
}

LoginPage.propTypes = {
  classes: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  errors: state.errors,
  auth: state.auth
});

export default withStyles(loginPageStyle)(
  connect(
    mapStateToProps,
    { loginUser }
  )(withRouter(LoginPage))
);

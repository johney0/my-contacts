/* eslint-disable react/prop-types */
import React from "react";
// @material-ui/core components
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
// core components
import CustomInput from "components/CustomInput/CustomInput.jsx";
import { FormGroup, Label, Input, Button, Row, FormText } from "reactstrap";
import CardBody from "components/Card/CardBody.jsx";
const loginFormBody = props => {
  const { classes, errors, emailValue, passwordValue, onChange } = props;
  return (
    <CardBody>
      {errors.login && <div className="text-danger">{errors.login}</div>}
      <FormGroup>
        <Label for="email">email</Label>
        <Input
          type="text"
          name="email"
          value={emailValue}
          id="email"
          placeholder="email"
          onChange={onChange}
          className={{ "is-invalid": errors.email }}
        />
      </FormGroup>
      {errors.email && <div className="text-danger">{errors.email}</div>}
      <FormGroup>
        <Label for="password">Password</Label>
        <Input
          type="password"
          name="password"
          value={passwordValue}
          id="password"
          placeholder="password"
          onChange={onChange}
        />
      </FormGroup>
      {errors.password && <div className="text-danger">{errors.password}</div>}

      {/* <CustomInput
        labelText="Email..."
        id="email"
        name="email"
        className={{ "is-invalid": errors.email }}
        onchange={onChange}
        //value={emailValue}
        formControlProps={{
          fullWidth: true
        }}
        inputProps={{
          type: "email",
          endAdornment: (
            <InputAdornment position="end">
              <Email className={classes.inputIconsColor} />
            </InputAdornment>
          )
        }}
      /> */}

      {/* <CustomInput
        labelText="Password"
        id="pass"
        name="password"
        onChange={onChange}
        value={passwordValue}
        className={{ "is-invalid": errors.password }}
        formControlProps={{
          fullWidth: true
        }}
        inputProps={{
          type: "password",
          endAdornment: (
            <InputAdornment position="end">
              <Icon className={classes.inputIconsColor}>lock_outline</Icon>
            </InputAdornment>
          )
        }}
      /> */}
    </CardBody>
  );
};

export default loginFormBody;

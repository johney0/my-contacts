/* eslint-disable react/prop-types */
import React from "react";
// core components
import Button from "components/CustomButtons/Button.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import LoginFormHeader from "./loginFormHeader";
import LoginFormBody from "./LoginFormBody";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import { Link } from "react-router-dom";

import Card from "components/Card/Card.jsx";

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cardAnimaton: "cardHidden"
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }
  render() {
    const {
      classes,
      errors,
      emailValue,
      passwordValue,
      onChange,
      submit
    } = this.props;
    return (
      <div className={classes.container}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={4}>
            <Card className={classes[this.state.cardAnimaton]}>
              <form onSubmit={submit} className={classes.form}>
                <p className={classes.divider}>Or Be Classical</p>
                <LoginFormBody
                  classes={classes}
                  errors={errors}
                  emailValue={emailValue}
                  passwordValue={passwordValue}
                  onChange={onChange}
                />
                <CardFooter className={classes.cardFooter}>
                  <GridContainer justify="center">
                    <Button type="submit" simple color="primary" size="lg">
                      Get started
                    </Button>
                    <GridItem>
                      <GridContainer>
                        <GridItem xs={6} sm={6} md={6} style={{ padding: 0 }}>
                          <p>New to Contacts ?</p>
                        </GridItem>
                        <GridItem xs={6} sm={6} md={6}>
                          <Link to="/register-page">
                            <Button
                              style={{ margin: 0, padding: 0 }}
                              simple
                              color="primary"
                              size="lg"
                            >
                              Create an account
                            </Button>
                          </Link>
                        </GridItem>
                      </GridContainer>
                    </GridItem>
                  </GridContainer>
                </CardFooter>
              </form>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default LoginForm;

const mongoose = require('mongoose'),
 bodyParser = require('body-parser'),
 express = require('express'),
 passport = require('passport'),
userRoute = require('./routes/userRoute'),
contactRoute = require('./routes/userContact'),
 path = require('path');

require('dotenv').config()

// let database;
// if(process.env.NODE_ENV === 'production'){
//     database = process.env.MONGO_ATLAS
// } else {
//  mongodb://localhost:27017/contacts   database = db.DB
// }

    mongoose.connect(process.env.MONGO_ATLAS
, { useNewUrlParser: true})
    .then(() => console.log('mongodb connected'))
    .catch(err => console.log(err))

const app = express()

// app.use(passport.initialize());
// require('./passport')(passport)

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

app.use('/api/users', userRoute)
app.use('/api/users', contactRoute)



// serve static asset if in prod
// if(process.env.NODE_ENV === 'production'){
//     // set static folder
//     app.use(express.static('client/build'))
//     app.use(bodyParser.urlencoded({extended:false}))
//     app.use(bodyParser.json())

//     // app.use ('*', (req, res) => { 
//     //     if (req.headers['x-forwarded-proto'] !== 'https') {
//     //         return res.redirect(['https://', req.get('Host'), req.url].join(''));
//     //     }
//     //     return next();
//     // })

//     app.use(function(req, res, next) {
//         if((!req.secure) && (req.get('X-Forwarded-Proto') !== 'https')) {
//             res.redirect('https://' + 'airtimehub.herokuapp.com' + req.url);
//         }
//         else
//             next();
//     });

//     app.get('*', (req, res) => {
//         res.header('Authorization' , process.env.JWT_LOGIN_KEYS);
//         res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
//     })
// }

const port = process.env.PORT || 5000;
app.listen(port, ()=> console.log(`server started on port${port}`))